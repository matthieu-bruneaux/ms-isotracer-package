### * Description

# Script to generate the figures for case study Li 2017

### * Setup

library(isotracer)
library(tidyverse)
library(magrittr)
library(ggplot2)
library(ggdist)
library(here)
library(grid)
library(latex2exp)
library(RColorBrewer)

FIG_DIR <- file.path(here::here(), "current-manuscript/figures")
SRC_DIR <- file.path(here::here(), "current-manuscript/figures/source")

set.seed(4)
n_cores <- min(2, parallel::detectCores())
n_chains <- max(n_cores, 4)
run_mcmc <- function(...) {
    isotracer:::run_mcmc(..., cores = n_cores, chains = n_cores,
                         seed = 43)
}

DEBUG <- FALSE

COLS <- setNames(brewer.pal(5, "Set2"),
                 c("CPN60A", "PGK1", "PSBA", "RBCL", "THI1"))
COLS_LIGHT <- setNames(adjustcolor(COLS, alpha.f = 0.3),
                       c("CPN60A", "PGK1", "PSBA", "RBCL", "THI1"))
GRID_COL <- grey(0.94)
GRID_LWD <- 1

### * Load and prepare data

m <- readRDS(file.path(SRC_DIR, "z-cache-case-study-li-2017-model.rds"))
run <- readRDS(file.path(SRC_DIR, "z-cache-case-study-li-2017-run.rds"))
pred <- readRDS(file.path(SRC_DIR, "z-cache-case-study-li-2017-pred.rds"))

### * Helper functions

### ** add_panel_label()

add_panel_label <- function(label, cex = 1) {
    x <- unit(0, "npc") + unit(0.5, "strwidth", "X") + unit(1/2, "strwidth", label)
    y <- unit(1, "npc") - unit(1.5, "strheight", "X")
    grid.text(label = label, x, y, gp = gpar(cex = cex))
}

### ** intersection_line_segment()

#' NOTE: This function does not work for vertical line (i.e. if diff(x_line) == 0).
#' 
#' All parameters are vectors of length 2.
#' x_segment and y_segment define a segment.
#' x_line and y_line define a ray starting at xline[1], y_line[1]
#'
#' @return NULL if the line and segment are parallel or do not meet; a (x,y)
#'     vector with the intersection point otherwise.
intersection_line_segment <- function(x_line, y_line, x_segment, y_segment) {
    stopifnot(diff(x_line) != 0)
    slope_line <- diff(y_line)/diff(x_line)
    if (diff(x_segment) != 0) {
        slope_segment <- diff(y_segment)/diff(x_segment)
        if (slope_line == slope_segment) { return(NULL) }
        intercept_line <- y_line[1] - slope_line * x_line[1]
        intercept_segment <- y_segment[1] - slope_segment * x_segment[1]
        x_intersect <- (intercept_line - intercept_segment) / (slope_segment - slope_line)
        y_intersect <- slope_line * x_intersect + intercept_line
        x_segment <- sort(x_segment)
        if ((x_intersect >= x_segment[1] & x_intersect <= x_segment[2]) &
            ((diff(x_line) > 0 & x_intersect > min(x_line)) |
             (diff(x_line) < 0 & x_intersect < max(x_line)))) {
            return(c(x_intersect, y_intersect))
        }
        return(NULL)
    } else {
        if ((diff(x_line) > 0 & min(x_line) < x_segment[1]) |
            (diff(x_line) < 0 & max(x_line) > x_segment[1])) {
            intercept_line <- y_line[1] - slope_line * x_line[1]
            y_intersect <- slope_line * x_segment[1] + intercept_line
            y_segment <- sort(y_segment)
            if (y_intersect >= y_segment[1] & y_intersect <= y_segment[2]) {
                return(c(x_segment[1], y_intersect))
            }
        }
    }
    return(NULL)
}

### ** intersections_line_rect()

#' All parameters are vectors of length 2.
#' x_segment and y_segment define a segment.
#' x_rect and y_rect define an axis-aligned rectangle.
#'
#' @return A list with the intersection coordinates
intersections_line_rect <- function(x_line, y_line, x_rect, y_rect) {
    segments <- list(list(x_rect, rep(y_rect[1], 2)),
                     list(x_rect, rep(y_rect[2], 2)),
                     list(rep(x_rect[1], 2), y_rect),
                     list(rep(x_rect[2], 2), y_rect))
    intersections <- lapply(segments, function(s) {
        x_segment <- s[[1]]; y_segment <- s[[2]]
        intersection_line_segment(x_line, y_line, x_segment, y_segment)
    })
    return(intersections[!sapply(intersections, is.null)])
}

intersections_line_rect(c(0, 1), c(0, 1), c(2, 3), c(0, 5))
intersections_line_rect(c(0, 1), c(0, 1), c(2, 3), c(5, 10))

### ** ray_hit_rect()

ray_hit_rect <- function(x_line, y_line, x_rect, y_rect) {
    hits <- intersections_line_rect(x_line = x_line, y_line = y_line,
                                    x_rect = x_rect, y_rect = y_rect)
    if (length(hits) == 0) return(hits)
    if (length(hits) == 1) return(hits[[1]])
    dist <- lapply(hits, function(h) {
        h0 <- c(x_line[1], y_line[1])
        sum((h - h0)^2)
    })
    first <- which.min(dist)
    return(hits[[first]])
}

### * draw_topology()

draw_topology <- function(model, run) {
    # Settings
    padding <- unit(1/2, "strwidth", "X")
    alpha_fill <- 0.5
    col_default <- adjustcolor("skyblue", alpha.f = alpha_fill)
    col_steady <- adjustcolor("grey", alpha.f = alpha_fill)
    radius <- 0.7
    legend_width <- 0.2
    legend_height <- 0.2
    legend_columns <- c(0.2, 0.3, 0.5)
    # Data viewport has xlim = c(-1, 1) and ylim = c(-1, 1)
    pushViewport(dataViewport(xscale = c(-1, 1), yscale = c(-1, 1)))
    # Debug
    if (DEBUG) {
        grid.rect(gp = gpar(lwd = 4, col = "magenta"))
        grid.text("topology", gp = gpar(col = "magenta", cex = 3))
    }
    # Prepare boxes information
    comps <- c("RBCL", "CPN60A", "PSBA", "THI1", "PGK1")
    angles <- -seq(0, 2*pi, length.out = 6)[1:5] + 1.5*pi/2
    boxes <- tibble(x = c(0, radius * cos(angles)),
                    y = c(0, radius * sin(angles)),
                    label = c("medium*", comps),
                    fill = c(col_steady, COLS_LIGHT[comps]))
    boxes$width <- lapply(boxes$label, function(l) {
        unit(1, "strwidth", l) + 2 * padding })
    boxes$height <- lapply(boxes$label, function(l) {
        unit(1, "strheight", l) + 2 * padding })
    print(boxes)
    # Draw connectors
    for (i in 2:nrow(boxes)) {
        x0 <- 0; y0 <- 0
        x1 <- boxes$x[i]; y1 <- boxes$y[i]
        width_nat_f <- convertWidth(boxes$width[[1]], "native", TRUE)
        height_nat_f <- convertHeight(boxes$height[[1]], "native", TRUE)
        width_nat_t <- convertWidth(boxes$width[[i]], "native", TRUE)
        height_nat_t <- convertHeight(boxes$height[[i]], "native", TRUE)
        hit_from <- ray_hit_rect(c(x0, x1), c(y0, y1),
                                 c(-1/2*width_nat_f, 1/2*width_nat_f),
                                 c(-1/2*height_nat_f, 1/2*height_nat_f))
        hit_to <- ray_hit_rect(c(x0, x1), c(y0, y1),
                               c(x1-1/2*width_nat_t, x1+1/2*width_nat_t),
                               c(y1-1/2*height_nat_t, y1+1/2*height_nat_t))
        if (DEBUG) {
            grid.lines(c(x0, x1), c(y0, y1), default.units = "native",
                       gp = gpar(col = "magenta"))
            grid.points(hit_to[1], hit_to[2], gp = gpar(col = "magenta"))
        }
        grid.lines(c(hit_from[1], hit_to[1]), c(hit_from[2], hit_to[2]),
                   default.units = "native",
                   arrow = arrow(length = unit(1, "strwidth", "X"), type = "closed"))
    }
    # Draw boxes
    for (i in seq_len(nrow(boxes))) {
        x <- boxes$x[i]; y <- boxes$y[i]
        width <- boxes$width[[i]]; height <- boxes$height[[i]]
        grid.roundrect(x, y, width = width, height = height,
                       default.units = "native", gp = gpar(fill = boxes$fill[i]))
        grid.text(boxes$label[i], x, y, default.units = "native")
    }
    # Topology legend
    grid.text(x = 0.5, y = unit(1, "npc") - 1/2 * unit(1, "lines"),
              label = "(* = steady state)")
    # Return
    popViewport()
}

## DEBUG <- TRUE
## grid.newpage()
## draw_topology(m, run)

### * draw_trajectories()

draw_trajectories <- function(model, pred) {
    # Settings
    header_mult_factor <- 1
    xlim <- extendrange(c(0, 5), f = 0.08)
    ylim <- extendrange(c(0, 1), f = 0.08)
    envelope_gp <- gpar(fill = grey(0.9), col = "black")
    header_fill <- grey(0.96)
    comps <- c("RBCL", "PSBA")
    # Viewport
    widths <- unit(c(header_mult_factor, 1, 1, 3),
                   c("lines", "null", "null", "lines"))
    heights <- unit(c(header_mult_factor, 1, 1, 2.5),
                    c("lines", "null", "null", "lines"))
    pushViewport(viewport(layout = grid.layout(nrow = 4, ncol = 4,
                                               widths = widths, heights = heights)))
    # Debug
    if (DEBUG) {
        grid.rect(gp = gpar(lwd = 4, col = "chartreuse"))
        grid.text("trajectories", gp = gpar(col = "chartreuse", cex = 3))
    }
    # Headers
    pushViewport(viewport(layout.pos.row = 1, layout.pos.col = 2))
    grid.rect(gp = gpar(fill = header_fill))
    grid.text(comps[1])
    popViewport()
    pushViewport(viewport(layout.pos.row = 1, layout.pos.col = 3))
    grid.rect(gp = gpar(fill = header_fill))
    grid.text(comps[2])
    popViewport()
    pushViewport(viewport(layout.pos.row = 2, layout.pos.col = 1))
    grid.rect(gp = gpar(fill = header_fill))
    grid.text("Leaf 3", rot = 90)
    popViewport()
    pushViewport(viewport(layout.pos.row = 3, layout.pos.col = 1))
    grid.rect(gp = gpar(fill = header_fill))
    grid.text("Leaf 7", rot = 90)
    popViewport()
    # Trajectories
    leaves <- c("leaf_3", "leaf_7")
    for (i in 1:2) {
        for (j in 1:2) {
            pushViewport(dataViewport(xscale = xlim, yscale = ylim,
                                      layout.pos.row = i + 1, layout.pos.col = j + 1))
            grid.rect()
            # Grid
            for (k in seq(0, 1, by = 0.2)) {
                grid.lines(x = c(0, 5), y = rep(k, 2), default.units = "native",
                           gp = gpar(col = GRID_COL, lwd = GRID_LWD))
            }
            for (k in 0:5) {
                grid.lines(x = rep(k, 2), y = c(0, 1), default.units = "native",
                           gp = gpar(col = GRID_COL, lwd = GRID_LWD))
            }
            # Predictions
            p <- pred$prediction[[which(groups(pred)$leaf_id == leaves[i])]] %>%
                filter(compartment == comps[j]) %>%
                arrange(time)
            grid.polygon(c(p$time, rev(p$time)), c(p$prop_low, rev(p$prop_high)),
                         default.units = "native",
                         gp = gpar(fill = COLS_LIGHT[comps[j]],
                                   col = COLS[comps[j]]))
            # Observations
            datapoint_gp <- gpar(col = adjustcolor(grey(0.1), alpha.f = 0.5),
                                 fill = adjustcolor(COLS[comps[j]], alpha.f = 0.7),
                                 cex = 0.8)
            init <- model$initial[[which(groups(model)$leaf_id == leaves[i])]] %>%
                filter(compartment == comps[j])
            obs <- model$observations[[which(groups(model)$leaf_id == leaves[i])]] %>%
                filter(compartment == comps[j])
            grid.points(0, init$proportion, pch = 21, gp = datapoint_gp)
            grid.points(obs$time, obs$proportion, pch = 21, gp = datapoint_gp)
            if (j == 2) grid.yaxis(main = FALSE)
            if (i == 2) grid.xaxis()
            popViewport()            
        }
    }
    # Axes
    pushViewport(viewport(layout.pos.row = 4, layout.pos.col = 2:3))
    grid.text("Time since medium switch (days)", y = 0, vjust = 0)
    popViewport()
    pushViewport(viewport(layout.pos.row = 2:3, layout.pos.col = 4))
    grid.text("Fraction labelled", rot = 90, x = 1, vjust = 0)
    popViewport()
    # Return
    popViewport()
}

## DEBUG <- TRUE
## grid.newpage()
## draw_trajectories(m, pred)

### * draw_posteriors()

draw_posteriors <- function(model, run) {
    # Settings
    header_mult_factor <- 1
    header_fill <- grey(0.96)
    ordered_prots <- c("RBCL", "CPN60A", "PSBA", "THI1", "PGK1")
    xlim <- c(0.5, 3.5)
    ylim_lambda <- c(-2.3, 1.2) # log10-transformed
    ylim_turnover <- c(-0.5, 2) # log10-transformed
    yaxis_lambda_at <- c(0.01, 0.1, 1, 10) # native
    yaxis_turnover_at <- c(0.5, 5, 50) # native
    datapoint_cex <- 0.6
    # Calculations
    lambdas <- list()
    turnovers <- list()
    for (p in ordered_prots) {
        for (l in c("leaf_3", "leaf_5", "leaf_7")) {
            lambda <- paste0("lambda_", p, "|", l)
            turnover <- paste0("turnover_", p, "|", l)
            lambdas[[lambda]] <- summary(run[, lambda])$quantiles
            lambdas[[lambda]]["mean"] <- mean(unlist(run[, lambda]))
            turnovers[[turnover]] <- summary(1/run[, lambda])$quantiles
            turnovers[[turnover]]["mean"] <- mean(unlist(1/run[, lambda]))
        }
    }
    # Viewport
    widths <- unit(c(header_mult_factor + 1, rep(1, 5), 3),
                   c("lines", rep("null", 5), "lines"))
    heights <- unit(c(header_mult_factor, 1, 1, 2.5),
                    c("lines", "null", "null", "lines"))
    pushViewport(viewport(layout = grid.layout(nrow = 4, ncol = 7,
                                               widths = widths, heights = heights)))
    # Debug
    if (DEBUG) {
        grid.rect(gp = gpar(lwd = 4, col = "orange"))
        grid.text("posteriors", gp = gpar(col = "orange", cex = 3))
    }
    # Headers
    for (i in seq_along(ordered_prots)) {
        pushViewport(viewport(layout.pos.row = 1, layout.pos.col = i + 1))
        grid.rect(gp = gpar(fill = header_fill))
        grid.text(ordered_prots[i])
        popViewport()
    }
    pushViewport(viewport(layout.pos.row = 2, layout.pos.col = 1))
    grid.rect(gp = gpar(fill = header_fill))
    grid.text("Loss rate", x = unit(0.5, "npc") - 1/2 * unit(1, "lines"), rot = 90)
    grid.text(TeX("coefficient $\\lambda$"),
              x = unit(0.5, "npc") + 1/2 * unit(1, "lines"), rot = 90)
    popViewport()
    pushViewport(viewport(layout.pos.row = 3, layout.pos.col = 1))
    grid.rect(gp = gpar(fill = header_fill))
    grid.text("Turnover time", rot = 90)
    popViewport()
    # Lambdas
    for (i in seq_along(ordered_prots)) {
        pushViewport(dataViewport(xscale = xlim, yscale = ylim_lambda,
                                  layout.pos.row = 2, layout.pos.col = i + 1))
        grid.rect()
        # Grid
        for (k in yaxis_lambda_at) {
            grid.lines(x = c(1, 3), y = log10(rep(k, 2)), default.units = "native",
                       gp = gpar(col = GRID_COL, lwd = GRID_LWD))
        }
        for (k in 1:3) {
            grid.lines(x = rep(k, 2), y = log10(range(yaxis_lambda_at)),
                       default.units = "native",
                       gp = gpar(col = GRID_COL, lwd = GRID_LWD))
        }
        for (j in 1:3) {
            p <- ordered_prots[i]
            l <- c("leaf_3", "leaf_5", "leaf_7")[j]
            lambda <- paste0("lambda_", p, "|", l)
            mean <- lambdas[[lambda]]["mean"]
            low <- lambdas[[lambda]]["2.5%"]
            high <- lambdas[[lambda]]["97.5%"]
            # Data
            grid.lines(rep(j, 2), log10(c(low, high)), default.units = "native",
                       gp = gpar(col = COLS[p], lwd = 3))
            grid.points(j, log10(mean), pch = 21,
                        gp = gpar(col = COLS[p], fill = COLS[p], cex = datapoint_cex))
        }
        if (i == 5) grid.yaxis(main = FALSE,
                               at = log10(yaxis_lambda_at),
                               label = yaxis_lambda_at)
        popViewport()
    }
    # Turnovers
    for (i in seq_along(ordered_prots)) {
        pushViewport(dataViewport(xscale = xlim, yscale = ylim_turnover,
                                  layout.pos.row = 3, layout.pos.col = i + 1))
        grid.rect()
        # Grid
        for (k in yaxis_turnover_at) {
            grid.lines(x = c(1, 3), y = log10(rep(k, 2)), default.units = "native",
                       gp = gpar(col = GRID_COL, lwd = GRID_LWD))
        }
        for (k in 1:3) {
            grid.lines(x = rep(k, 2), y = log10(range(yaxis_turnover_at)),
                       default.units = "native",
                       gp = gpar(col = GRID_COL, lwd = GRID_LWD))
        }
        for (j in 1:3) {
            p <- ordered_prots[i]
            l <- c("leaf_3", "leaf_5", "leaf_7")[j]
            turnover <- paste0("turnover_", p, "|", l)
            mean <- turnovers[[turnover]]["mean"]
            low <- turnovers[[turnover]]["2.5%"]
            high <- turnovers[[turnover]]["97.5%"]
            # Data
            grid.lines(rep(j, 2), log10(c(low, high)), default.units = "native",
                       gp = gpar(col = COLS[p], lwd = 3))
            grid.points(j, log10(mean), pch = 21,
                        gp = gpar(col = COLS[p], fill = COLS[p], cex = datapoint_cex))
        }
        if (i == 5) grid.yaxis(main = FALSE,
                               at = log10(yaxis_turnover_at),
                               label = yaxis_turnover_at)
        grid.xaxis(at = 1:3, label = c("L3", "L5", "L7"))
        popViewport()
    }
    # Axes
    pushViewport(viewport(layout.pos.row = 4, layout.pos.col = 2:6))
    grid.text("Leaf", y = 0, vjust = 0)
    popViewport()
    pushViewport(viewport(layout.pos.row = 2:3, layout.pos.col = 7))
    grid.text(TeX("1/day"), rot = 90, x = 1, y = 0.75, vjust = 0)
    grid.text("day", rot = 90, x = 1, y = 0.25, vjust = 0)
    popViewport()
    # Return
    popViewport()
}

## DEBUG <- TRUE
## grid.newpage()
## draw_posteriors(m, run)

### * draw_figure()

draw_figure <- function(model, run, pred, padding = 0) {
    cex_panel_label <- 1.2
    # Prepare page
    grid.newpage()
    widths <- c(0.8, 1)
    pushViewport(viewport(layout = grid.layout(nrow = 2, ncol = 2,
                                               widths = widths)))
    # Topology
    pushViewport(viewport(layout.pos.row = 1, layout.pos.col = 1))
    pushViewport(viewport(width = unit(1, "npc") - unit(padding, "in"),
                          height = unit(1, "npc") - unit(padding, "in")))
    draw_topology(model, run)
    popViewport()
    add_panel_label("(A)", cex = cex_panel_label)
    popViewport()
    # Predicted trajectories
    pushViewport(viewport(layout.pos.row = 1, layout.pos.col = 2))
    pushViewport(viewport(width = unit(1, "npc") - unit(padding, "in"),
                          height = unit(1, "npc") - unit(padding, "in")))
    draw_trajectories(model, pred)
    popViewport()
    add_panel_label("(B)", cex = cex_panel_label)
    popViewport()
    # Posteriors
    pushViewport(viewport(layout.pos.row = 2, layout.pos.col = 1:2))
    pushViewport(viewport(width = unit(1, "npc") - unit(padding, "in"),
                          height = unit(1, "npc") - unit(padding, "in")))
    draw_posteriors(model, run)
    popViewport()
    add_panel_label("(C)", cex = cex_panel_label)
    popViewport()
    # Return
    popViewport()
}

### * Save

DEBUG <- FALSE
adj_mult <- 0.8
cairo_pdf(file.path(FIG_DIR, "figure_100_li-2017.pdf"),
          width = 9 * adj_mult, height = 9 * adj_mult, family = "serif")
draw_figure(model = m, run = run, pred = pred, padding = 0.35)
dev.off()

