### * Description

# Script to generate the figures for case study McRoy 1970

### * Setup

library(isotracer)
library(tidyverse)
library(magrittr)
library(ggplot2)
library(ggdist)
library(here)
library(grid)
library(latex2exp)
library(RColorBrewer)

FIG_DIR <- file.path(here::here(), "current-manuscript/figures")
SRC_DIR <- file.path(here::here(), "current-manuscript/figures/source")

DEBUG <- FALSE
GRID_COL <- grey(0.94)
GRID_LWD <- 1

### * Load and prepare data

m <- readRDS(file.path(SRC_DIR, "z-cache-case-study-mcroy-1970-model.rds"))
run <- readRDS(file.path(SRC_DIR, "z-cache-case-study-mcroy-1970-run.rds"))
pred <- readRDS(file.path(SRC_DIR, "z-cache-case-study-mcroy-1970-pred.rds"))
flows <- readRDS(file.path(SRC_DIR, "z-cache-case-study-mcroy-1970-flows.rds"))

### * Helper functions

### ** add_panel_label()

add_panel_label <- function(label, cex = 1) {
    x <- unit(0, "npc") + unit(0.5, "strwidth", "X") + unit(1/2, "strwidth", label)
    y <- unit(1, "npc") - unit(1.5, "strheight", "X")
    grid.text(label = label, x, y, gp = gpar(cex = cex))
}

### * draw_sankey()

draw_sankey <- function(model, flows) {
    # Calculate total flows
    treatments <- groups(model)
    treatments$flows <- rep(list(NULL), 4)
    for (i in 1:nrow(treatments)) {
        f <- flows %>% filter_by_group(light_treatment == treatments$light_treatment[[i]],
                                       addition_site == treatments$addition_site[[i]]) %>%
        pull(flows) %>% lapply(ungroup) %>%
        bind_rows %>% group_by(from, to) %>%
        summarize(mean_flow = mean(average_flow), sd_flow = sd(average_flow),
                  .groups = "drop")
        treatments$flows[[i]] <- f
    }
    f_light <- treatments %>% filter(light_treatment == "light") %>%
        pull(flows) %>% bind_rows %>% group_by(from, to) %>%
        summarize(flow = sum(mean_flow), .groups = "drop") %>%
        na.omit()
    f_dark <- treatments %>% filter(light_treatment == "dark") %>%
        pull(flows) %>% bind_rows %>% group_by(from, to) %>%
        summarize(flow = sum(mean_flow), .groups = "drop") %>%
        na.omit()
    # Viewports
    pushViewport(viewport(layout = grid.layout(ncol = 2)))
    pushViewport(viewport(layout.pos.col = 2))
    draw_one_sankey(flows = f_light, adj_connectors = 1, title = "Light")
    popViewport()
    pushViewport(viewport(layout.pos.col = 1))
    draw_one_sankey(flows = f_dark, adj_connectors = 1, title = "Dark")
    popViewport()
    # Debug
    if (DEBUG) {
        grid.rect(gp = gpar(lwd = 4, col = "magenta", fill = NA))
        grid.text("sankey", gp = gpar(col = "magenta", cex = 3))
    }
    # Return
    popViewport()
}

### * draw_one_sankey()

draw_one_sankey <- function(flows, adj_connectors = 1, title = "My title") {
    # Settings
    adj_connectors <- 1/80
    title_fill <- grey(0.95)
    comps <- c("lower_water" = "Lower water",
               "roots_rhizome" = "Roots + rhizome",
               "leaves_stem" = "Leaves + stem",
               "upper_water" = "Upper water") # Order is hard-coded and should
                                              # not be changed!
    arrow <- NULL
    # Viewport
    heights <- unit(c(1, 1), c("null", "lines"))
    pushViewport(viewport(layout = grid.layout(nrow = 2, heights = heights)))
    # Title
    pushViewport(viewport(layout.pos.row = 2))
    grid.rect(gp = gpar(fill = title_fill))
    grid.text(title)
    popViewport()
    # Sankey
    pushViewport(dataViewport(layout.pos.row = 1, xscale = c(0, 1), yscale = c(0.5, 4.5)))
    ## Flow upper water -> leaves
    f_up_to_ls <- flows %>% filter(from == "upper_water" & to == "leaves_stem") %>%
        pull(flow)
    grid.lines(c(0.5, 0.5), c(4, 3), default.units = "native",
               gp = gpar(lwd = f_up_to_ls * adj_connectors, lineend = 3),
               arrow = arrow)
    ## Flow lower water -> roots
    f_lw_to_rt <- flows %>% filter(from == "lower_water" & to == "roots_rhizome") %>%
        pull(flow)
    grid.lines(c(0.5, 0.5), c(1, 2), default.units = "native",
               gp = gpar(lwd = f_lw_to_rt * adj_connectors, lineend = 3),
               arrow = arrow)
    ## Flow leaves -> roots
    f_ls_to_rt <- flows %>% filter(from == "leaves_stem" & to == "roots_rhizome") %>%
        pull(flow)
    grid.bezier(x = c(0.5, 1, 1, 0.5), y = c(3, 3, 2, 2),
                default.units = "native",
                gp = gpar(lwd = f_ls_to_rt * adj_connectors, lineend = 3),
                arrow = arrow)
    ## Flow roots -> leaves
    f_rt_to_ls <- flows %>% filter(from == "roots_rhizome" & to == "leaves_stem") %>%
        pull(flow)
    grid.bezier(x = c(0.5, 0, 0, 0.5), y = c(2, 2, 3, 3),
                default.units = "native",
                gp = gpar(lwd = f_rt_to_ls * adj_connectors, lineend = 3),
                arrow = arrow)
    ## Place compartments
    for (i in 1:4) {
        grid.roundrect(x = 0.5, y = i, width = unit(1.1, "strwidth", comps[i]),
                       height = unit(1, "lines"), default.units = "native")
        grid.text(comps[i], x = 0.5, y = i, default.units = "native")
    }
    ## Add indicative arrows
    grid.lines(rep(0.5, 2), c(3.65, 3.35), default.units = "native",
               gp = gpar(col = "white", fill = "white"),
               arrow = arrow(length = unit(0.5, "strheight", "a"), type = "closed"))
    grid.lines(rep(0.5, 2), c(1.35, 1.65), default.units = "native",
               gp = gpar(col = "white", fill = "white"),
               arrow = arrow(length = unit(0.5, "strheight", "a"), type = "closed"))
    grid.lines(rep(0.86, 2), c(2.48, 2.46), default.units = "native",
               gp = gpar(col = "black", fill = "black"),
               arrow = arrow(length = unit(0.7, "strheight", "a"), type = "closed"))
    grid.lines(rep(0.14, 2), c(2.52, 2.54), default.units = "native",
               gp = gpar(col = "black", fill = "black"),
               arrow = arrow(length = unit(0.7, "strheight", "a"), type = "closed"))
    # Return
    popViewport()
    popViewport()
}

### * draw_ratios()

draw_ratios <- function(run) {
    # Settings
    ylim <- c(0.5, 5)
    xlim <- c(-1.7, 2.7) # log10-transformed
    xat <- c(0.1, 1, 10, 100) # native
    y_labels <- list("lw_to_rt" = c("Lower water", "roots/rhizome"),
                     "rt_to_ls" = c("Roots/rhizome", "leaves/stem"),
                     "ls_to_rt" = c("Leaves/stem", "roots/rhizome"),
                     "up_to_ls" = c("Upper water", "leaves/stem"))
    cex_ylab <- 0.8
    # Calculate ratios
    ratios <- list()
    ratios[["ls_to_rt"]] <- (run[, "upsilon_leaves_stem_to_roots_rhizome|light"]/
                             run[, "upsilon_leaves_stem_to_roots_rhizome|dark"])
    ratios[["lw_to_rt"]] <- (run[, "upsilon_lower_water_to_roots_rhizome|light"]/
                             run[, "upsilon_lower_water_to_roots_rhizome|dark"])
    ratios[["rt_to_ls"]] <- (run[, "upsilon_roots_rhizome_to_leaves_stem|light"]/
                             run[, "upsilon_roots_rhizome_to_leaves_stem|dark"])
    ratios[["up_to_ls"]] <- (run[, "upsilon_upper_water_to_leaves_stem|light"]/
                             run[, "upsilon_upper_water_to_leaves_stem|dark"])
    ratios <- lapply(ratios, function(x) {
        x <- c(summary(x)$quantiles, summary(x)$statistics)
        x[c("2.5%", "Mean", "97.5%")]
    })
    # Viewport
    widths <- unit(c(1, 1), c("strwidth", "null"), list("Roots/rhizome      ", NULL))
    heights <- unit(c(1, 3.5), c("null", "lines"))
    pushViewport(viewport(layout = grid.layout(nrow = 2, ncol = 2,
                                               widths = widths, heights = heights)))
    # Y labels
    pushViewport(dataViewport(xscale = c(0, 1), yscale = ylim,
                              layout.pos.col = 1, layout.pos.row = 1))
    for (i in seq_along(y_labels)) {
        y_labels[[i]][1] <- TeX(paste0(y_labels[[i]][1], "$\\rightarrow$"))
        grid.text(y_labels[[i]][1], 0.5,
                  unit(i, "native") + 1/2 * unit(cex_ylab, "lines"),
                  gp = gpar(cex = cex_ylab))
        grid.text(y_labels[[i]][2], 0.5,
                  unit(i, "native") - 1/2 * unit(cex_ylab, "lines"),
                  gp = gpar(cex = cex_ylab))
    }
    popViewport()
    # X legend
    pushViewport(viewport(layout.pos.row = 2, layout.pos.col = 2))
    grid.text("Ratio of uptake rate coefficients\n(in light) / (in dark)",
              y = 0, vjust = 0,
              gp = gpar(cex = cex_ylab))
    popViewport()
    # Data
    pushViewport(dataViewport(xscale = xlim, yscale = ylim,
                              layout.pos.col = 2, layout.pos.row = 1))
    grid.lines(rep(0, 2), c(0.75, 4.25), default.units = "native",
               gp = gpar(col = "lightgrey"))
    for (i in seq_along(y_labels)) {
        r <- ratios[[names(y_labels)[i]]]
        grid.lines(log10(r[c("2.5%", "97.5%")]), rep(i, 2), default.units = "native",
                   gp = gpar(lwd = 2))
        grid.points(log10(r["Mean"]), i, pch = 21,
                    gp = gpar(col = "black", fill = "black", cex = 0.7))
    }
    grid.yaxis(at = 1:4, label = FALSE)
    grid.xaxis(at = log10(xat), label = xat, gp = gpar(cex = cex_ylab))
    popViewport()
    # Debug
    if (DEBUG) {
        grid.rect(gp = gpar(lwd = 4, col = "chartreuse", fill = NA))
        grid.text("ratios", gp = gpar(col = "chartreuse", cex = 3))
    }
    # Return
    popViewport()
}

### * draw_figure()

draw_figure <- function(model, run, pred, flows, padding = 0) {
    cex_panel_label <- 1.2
    # Prepare page
    grid.newpage()
    widths <- c(1, 1)
    pushViewport(viewport(layout = grid.layout(nrow = 1, ncol = 2,
                                               widths = widths)))
    # Sankey plot
    pushViewport(viewport(layout.pos.row = 1, layout.pos.col = 1))
    pushViewport(viewport(width = unit(1, "npc") - unit(padding, "in"),
                          height = unit(1, "npc") - unit(padding, "in")))
    draw_sankey(model, flows)
    popViewport()
    add_panel_label("(A)", cex = cex_panel_label)
    popViewport()
    # Ratios light/day
    pushViewport(viewport(layout.pos.row = 1, layout.pos.col = 2))
    pushViewport(viewport(width = unit(1, "npc") - unit(padding, "in"),
                          height = unit(1, "npc") - unit(padding, "in")))
    draw_ratios(run)
    popViewport()
    add_panel_label("(B)", cex = cex_panel_label)
    popViewport()
    # Return
    popViewport()
}

### * Save

DEBUG <- FALSE
adj_mult <- 0.8
cairo_pdf(file.path(FIG_DIR, "figure_200_mcroy-1970.pdf"),
          width = 9 * adj_mult, height = 4.5 * adj_mult, family = "serif")
draw_figure(model = m, run = run, pred = pred, flows = flows, padding = 0.1)
dev.off()

