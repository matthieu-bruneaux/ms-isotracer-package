### * Setup

TOP_DIR = $(shell git rev-parse --show-toplevel)
TARGET = manuscript

### ** Colors

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://siongui.github.io/2016/02/18/makefile-echo-color-output/#id3
RED = "\\033[31m"
GREEN = "\\033[92m"
NC = "\\033[0m"

### * Rules

### ** help

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@printf "\n"
	@printf "Please use 'make <target>' where <target> is one of\n"
	@printf "\n"
	@sed -n 's/^## /    /p' $< | column -t -s ":"

### ** figures

## figures : generate the figures from R
.PHONY: figures
figures:
	@printf "\n"
	@printf "$(GREEN)*** Generating the figures ***$(NC)\n"
	@printf "\n"
	#Rscript figures/source/script.R
	Rscript figures/source/script-li-2017.R
	Rscript figures/source/script-mcroy-1970.R

### ** pdf

## pdf : build the pdf (main text)
.PHONY: pdf
pdf:
	@printf "\n"
	@printf "$(GREEN)*** Building the pdf (main text) ***$(NC)\n"
	@printf "\n"
	#Rscript -e 'Sweave("$(TARGET).Rnw")'
	pdflatex $(TARGET).tex
	bibtex $(TARGET)
	pdflatex $(TARGET).tex
	pdflatex $(TARGET).tex
	pdflatex $(TARGET).tex

### ** appendix

## appendix : generate the tutorials appendix
.PHONY: appendix
appendix:
	@printf "\n"
	@printf "$(GREEN)*** Generating the tutorials appendix ***$(NC)\n"
	@printf "\n"
	Rscript -e 'rmarkdown::render("appendix-tutorials/000-tutorials.Rmd")'
	cp appendix-tutorials/000-tutorials.pdf ./appendix.pdf

### ** pdf-with-appendix

## pdf-with-appendix : build the pdf with the tutorials appendix
.PHONY: pdf-with-appendix
pdf-with-appendix: pdf appendix
	@printf "\n"
	@printf "$(GREEN)*** Building the pdf with the appendix ***$(NC)\n"
	@printf "\n"
	pdfunite $(TARGET).pdf appendix.pdf $(TARGET)-with-appendix.pdf

### ** clean

## clean : remove all automatically generated files except the appendix
.PHONY: clean
clean: clean-tmp clean-pdf
	@printf "\n"
	@printf "$(GREEN)*** Complete cleaning ***$(NC)\n"
	@printf "\n"

# clean-tmp : remove the helper files automatically generated
.PHONY: clean-tmp
clean-tmp:
	@rm -f *.aux *.log *.out *.bbl *.blg *.dvi *.toc *.lof *.lot *.losupfig *.losuptab
	@rm -f tables/*.aux
	@rm -f diff.tex diff.pdf

# clean-pdf : remove the target pdf
.PHONY: clean-pdf
clean-pdf:
	@rm -f $(TARGET).pdf
	@rm -f $(TARGET)-with-appendix.pdf

# clean-figures : remove the figures that can be generated from R
.PHONY: clean-figures
clean-figures:
	@cd figures; rm -f figure_110_li2017-topo.pdf figure_120_li2017-traces.pdf figure_130_li2017-ppc-props.pdf figure_150_li2017-lambdas.pdf figure_170_li2017-sankey.pdf supfigure_140_li2017-ppc-sizes.pdf supfigure_160_li2017-turnover-times.pdf figure_210_mcroy1970-topo.pdf figure_220_mcroy1970-traces.pdf figure_230_mcroy1970-observations.pdf figure_240_mcroy1970-ppc-props.pdf figure_250_mcroy1970-sankey.pdf

# clean-appendix : remove the appendix pdfs
.PHONY: clean-appendix
clean-appendix:
	rm -f appendix.pdf
	rm -f appendix-tutorials/000-tutorials.pdf

### ** all

## all : clean, build manuscript and add existing appendix (but does not rebuild appendix)
.PHONY: all
all: clean pdf
	@make clean-tmp
	pdfunite $(TARGET).pdf appendix.pdf $(TARGET)-with-appendix.pdf

### ** diff

## diff : diff between manuscript.tex and snapshot.tex
.PHONY: diff
diff:
	@printf "\n"
	@printf "$(GREEN)*** Building the diff pdf ***$(NC)\n"
	@printf "\n"
	rm -f diff.*
	latexdiff snapshot.tex manuscript.tex > diff.tex
	Rscript .update-diff-preamble.R
	pdflatex diff.tex
	bibtex diff
	pdflatex diff.tex
	pdflatex diff.tex
	pdflatex diff.tex

