### * Description

# Modify preamble of the output of latexdiff to use customized options

### * Modifications

# Each element is c("original", "replacement")
MODIFS <- list(
    c("\\RequirePackage[normalem]{ulem}",
      "\\RequirePackage[normalem]{ulem}\n\\RequirePackage[dvips]{changebar}"),
    c("\\RequirePackage{color}\\definecolor{RED}{rgb}{1,0,0}\\definecolor{BLUE}{rgb}{0,0,1} %DIF PREAMBLE",
      "\\RequirePackage{color}\\definecolor{lightred}{rgb}{1,0.5,0.5} %DIF PREAMBLE"),
    c("\\providecommand{\\DIFaddtex}[1]{{\\protect\\color{blue}\\uwave{#1}}} %DIF PREAMBLE",
      "\\providecommand{\\DIFaddtex}[1]{{\\protect\\cbstart{\\protect\\color{red}{#1}\\cbend}}} %DIF PREAMBLE"),
    c("\\providecommand{\\DIFdeltex}[1]{{\\protect\\color{red}\\sout{#1}}}                      %DIF PREAMBLE",
      "\\providecommand{\\DIFdeltex}[1]{{\\protect\\cbstart{\\protect\\color{lightred}\\sout{#1}\\cbend}}}                      %DIF PREAMBLE"),
    c("pdftitle={R package isotracer}]{hyperref}",
      "pdftitle={R package isotracer (diff)}]{hyperref}"),
    c("\\tableofcontents", ""))

### * Main

FILE <- "diff.tex"
content <- readLines(FILE)
for (i in seq_along(MODIFS)) {
    index <- grep(MODIFS[[i]][1], content, fixed = TRUE)
    if (length(index) == 0) {
        stop("Pattern:\n", MODIFS[[i]][1], "\nnot found.")
    }
    content[index] <- MODIFS[[i]][2]
}
writeLines(content, FILE)
