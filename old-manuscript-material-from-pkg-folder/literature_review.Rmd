---
title: 'Tracer Addition Experiments Review'
bibliography: isotracer.bib
author: "Andrés López-Sepulcre and Matthieu Bruneaux"
date: "September 11, 2018"
output:
  html_document:
    fig_caption: yes
    force_captions: yes
    highlight: pygments
    number_sections: no
    toc: true
    toc_float: true
    theme: lumen
  word_document: default
  pdf_document:
    fig_caption: yes
    keep_tex: yes
    number_sections: no
csl: mee.csl
---

```{r setup, message = FALSE, warning = FALSE, echo = FALSE}
# Define an R6 class to store the information from each reference
# References are added using the $add() method of the class.
library(R6)
entryList = R6Class(classname = "entryList",
    public = list(
      initialize = function() {
        self$content = list()
    },
    add = function(doi, 
                   tracer = "NA", system = "NA", 
                   level = "NA", input = "NA", 
                   sampling = "NA", data = "no"){
        # Codes
        input_list <- c("sp" = "single pulse", "mp" = "multiple pulses", 
                        "pc" = "pulse-chase", "ci" = "continuous interval",
                        "NA" = NA)
        sampling_list <- c("ql" = "qualitative", "sq" = "semi-quantitative", 
                           "qt" = "**quantitative**", "NA" = NA)
        level_list <- c("o" = "organism", "c" = "community", 
                        "e" = "ecosystem", "NA" = NA)
        data_list <- c("tr" = "tracer (raw)", "tm" = "tracer (mean)",
                       "trs" = "*tracer (raw) + mass*", "tms" = "tracer (mean) + mass",
                       "no" = "", "NA" = NA)
        # Add content
        self$content[[paste0("@", doi)]] <- c(level_list[level], tracer, system, 
          input_list[input], sampling_list[sampling], data_list[data])
        # Return citation key
        return(paste0("[@", doi, "]"))
    },
    content = NULL))

# Initialize an empty "entryList" object
entries = entryList$new()
```

### Description
This document aims to review the literature of tracer addition experiments (isotopic or otherwise), making emphasis on the variety of questions and systems.

## Introduction

Tracer addition experiments have been used in a wide range of systems to answer questions ranging from the molecular and organismal to the community and ecosystem levels. In this review I will first give an overview of the types of questions and systems to which this methodology has been applied, and then provide a reference table of studies, classifying them along the following aspects of their data and design:

 - **Tracer**: Most studies use rare stable isotopes as tracers, but radioactive isotopes and variety of other tracers have been used, such as encapsulated DNA `r entries$add("Mora_2014", "encapsulated DNA", "plant-insect-bird", "c", "sp", "ql")`.
 - **Input**: There are three main types of additions:
    + *Single Pulse* (`sp`): A single addition of labelled resource. 
    + *Multiple Pulses* (`mp`): Same as above but several times.
    + *Pulse-chase* (`pc`). A single addition of labelled resource followed by the same ammount of non labelled resource.
    + *Continuous drip* (`ci`). An interval of continuous input followed by sampling with no input.
 - **Sampling**: It can occurr once or multiple times. Studies can be classified in:
    + *Qualitative* (`ql`): Only presence-absence of tracer
    + *Semi-quantitative* (`sq`): Single sampling but estimated quantity of tracer
    + *Quantitative* (`qt`): Time series that allows the quantification of flux rates.
 - **Level**: Studies have been made at the level of:
    + *Organismal* (`o`): At the molecular or tissue level
    + *Community* (`c`): Most of the time in the context two-species interactions (e.g. symbiosis).
    + *Ecosystem* (`e`): to sutdy the cycles of organig and inorganic mater.
 - **Data**: Whether there is available useable data on:
    + *Tracer only (raw)* (`tr`).
    + *Tracer only (mean)* (`tm`).
    + *Tracer (raw) and (bio)mass* (`trs`).
    + *Tracer (mean) and (bio)mass* (`tms`).

## Questions

### Organismal level

- Study of metabolic pathways `r entries$add("Bacher_2016","$^{13}CO_2$", "plants","o", "pc", "ql","tm")`
- Cold adaptation in *Drosophila* by increase of metabolic fluxes `r entries$add("Williams_2016","$^{13}C$-glucose","Drosophila","o", "ci","sq")` 
- Quantify the assimilation of nutrients `r entries$add("R_nnestad_2000", "$^{14}C$-protein hydrolysate", "fish", "o", "sp", "qt")` or pollutants `r entries$add("Croteau_2007")` by organisms
- Study of resource allocation in plants `r entries$add("McRoy_1970", "$^{32}P$", "plant", "o", "sp", "qt", "tm")` and `r entries$add("Simard_1997", "$^{13}CO_2$", "plant", "o", "pc", "qt", "tms")`
- Study of maternal transfer to offspring (placenta) in reptiles `r entries$add("Van_Dyke_2012", "$^{15}N$-leucine", "snakes", "o", "sp", "sq")` and fish `r entries$add("Morrison_2017", "$^{14}C$-glycine,  $^{14}C$-leucine", "fish","o", "sp", "ql")`

### Species interactions

- Symbiosis: exchange of resources between species, `r entries$add("Freeman_2013","$NaH^{13}CO_3$ $Na^{15}NO_3$",  "sponge-microbe", "c", "sp", "sq")`
- Efficiency of ant colony fungus farming and resource alocation `r entries$add("Shik_2018", "$^{13}C$-glucose,$^{15}NH_4$, $^{15}NO_3$", "ant-fungus", "c", "sp", "qt")`.

### Ecosystem function

Quantification nutrient fluxes and cycling in a variety of ecosystems:

- Streams. Perhaps the largest literature.
- Lakes
- Importance of sponge loop in coral reefs both in microcosms `r entries$add("Rix_2016", "$^{13}C$, $^{15}N$", "coral reef", "e", "sp", "sq")`, and the wild `r entries$add("de_Goeij_2013", "$^{13}C$, $^{15}N$", "coral reef", "e", "ci","qt", "tm")`
- Allocation of stored carbon to rhizosphere in boreal forest `r entries$add("CARBONE_2007", "$^{14}CO_2$","boreal forest", "e", "pc", "qt","tm")`
- Importance of mammal urine on tundra N cycle `r entries$add("Barthelemy_2017", "$^{15}N$-urea", "tundra", "e", "sp", "sq", "ts")`
- Role of bellowground partitioning in biodiversity-productivity relationship in grasslands `r entries$add("Jesch_2018", "$Li$, $Rb$, $^{15}N$,  $^2H_2O$, $H_2^{18}O$", "grassland", "e", "sp", "qt")`
- Role of earthworms on mocrobial soil respiration `r entries$add("Chang_2016", "$^{13}C$, $^{15}N$", "soil","e","sp","sq")`

## Studies table summary

```{r , echo = FALSE}
ref_table <- data.frame(do.call(rbind, entries$content))
knitr::kable(ref_table[order(ref_table[,1],ref_table[,4], ref_table[,5]),],
             col.names = c('Level','Tracer', 'System', 'Input', 'Sampling', 'Available Data'))
```

## Selected candidates

Our selection is based on the value of a given dataset as an example, of the
availability of biomasses, and if no biomasses are present on the possibility
to make educated guesses about their values (while being clear in the paper
when dummy data is used and no biological conclusions should be inferred from a
particular example).

For now our selected candidates are:

### Study 1

- A very simple system, at the organism level such as root/stem/leaves in
  plants, to introduce the package.
- Possibly using a radioactive element such that decay must be taken into
  account.
- Either **McRoy & Barsdate (1970)** or **Simard et al. (1997)**.

### Study 2

- Building up in complexity and introducing an ecosystem model.
- Possibly using split compartments to introduce refractory and active portions.
- One of the sponge or coral reef papers such as **de Goeij et al. (2013)**.

### Study 3

- The most complex model presented, which can be used to introduce model
  comparison
- A complex community such as the leaf-cutter ant colony from **Shik et
  al. (2018)**.

## Attic: Matthieu's miscellaneous notes

- Shik 2018: looks good
- Mora 2015: data available, but very specialised system?
- Freeman 2013: all raw data deposited on Dryad, but not found there -> I sent
  an email to the Dryad helpdesk.
- In Shik 2018: many references about the use of radioisotopes, natural heavy
  isotopes and isotopes enrichment (in introduction).

## Bibliography

```{r, warning=FALSE, message=FALSE, echo=FALSE} 
#write.bibtex(file="isotracer.bib")
```
