## Description

This repository contains the source files for the manuscript **"isotracer: An R package for the analysis of tracer addition experiments"** by Bruneaux and López-Sepulcre.

The [corresponding pdf
files](https://matthieu-bruneaux.gitlab.io/ms-isotracer-package/) are
built automatically every time some changes are pushed to the repository.


### How to contribute

You can clone the repository locally from your terminal (requires you to be a
member of the repository):

```
git clone git@gitlab.com:matthieu-bruneaux/ms-isotope-package.git
```

The repository is organised into sub-folders:

 - **current-manuscript/** contains the latest version of the manuscript
   (`manuscript.tex`). This is the file to edit if you want to modify the manuscript.
-  **2021-08-12_submission-MEE/2021-10-11_revisions-and-response/** contains the latest version of the response letter to MEE reviews (`response-letter.draft.tex`). This is the file to edit if you want to modify the response letter.

For ease of use, a global `Makefile` is available in the root directory. Type:

- `make all` to generate the latest version of the manuscript and of the response letter.
