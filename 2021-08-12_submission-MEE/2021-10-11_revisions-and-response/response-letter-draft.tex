%%% * Preamble

\documentclass[a4paper,12pt]{report}

\usepackage[utf8]{inputenc} % Character encoding (for accents)
\usepackage[top=3cm, bottom=3cm, left=3cm, right=2.5cm]{geometry} % Margins
\usepackage[authoryear]{natbib} % Citations
\usepackage{textcomp}
\usepackage{xcolor}
  \definecolor{revcolor}{HTML}{cd3333}
\usepackage{soul} % For highlighting
\usepackage[colorlinks=true, linkcolor=red, citecolor=black, bookmarks=true, pdftitle={Response to reviewers}]{hyperref}
\usepackage{bookmark}
\usepackage{amsmath} % For smallmatrix environment and to define new math operators
  \DeclareMathOperator{\logit}{logit}
  \DeclareMathOperator{\corr}{corr}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{siunitx}
\usepackage{multirow}
\usepackage{newfloat}
\usepackage{mdframed} % To frame quoted text

%%% ** New environment for quotes (reviewers' and our own)

% https://tex.stackexchange.com/questions/288551/change-quote-command-to-display-text-in-italics

% revquote is for reviewers comments
\newenvironment{revquote}{\bigbreak\itshape}
%               {\bigbreak\itshape\color{revcolor}\noindent}
               {\bigbreak\color{revcolor}\noindent}

% ourquote is for quoting the modified manuscript
\newmdenv[
  topline = false,
  bottomline = false
]{siderules}
               
\newenvironment{ourquote}
               {\begin{quote}\begin{siderules}}
               {\end{siderules}\end{quote}}

%%% ** New environment for sup. figures

\DeclareFloatingEnvironment[
  fileext=losupfig,
  listname={List of Supplementary Figures},
  name={Supplementary Figure},
  placement = tbhp]{supfigure}
\setcounter{supfigure}{3}
\renewcommand{\thesupfigure}{S\arabic{supfigure}}%

%%% * Main text

\begin{document}

%%% ** General statement from us

Dear Editor,

\quad

Thank you for the comments on our manuscript entitled ``\texttt{isotracer}: An R package for the analysis of tracer addition experiments'' (ref. MEE-21-08-630). We were delighted by the reviewers' positive feedback on our work and for the attention to detail that they demonstrated in their reviews. We are grateful for their suggestions and criticism, which we have considered carefully, and we have updated our manuscript to incorporate their suggestions. We are now ready to submit revised versions of the manuscript and of the package for your evaluation. The updated version of the package (1.0.4.9002) is publicly available on Gitlab (\url{https://matthieu-bruneaux.gitlab.io/isotracer/dev/}).

Please find below a point-by-point response to the reviewers' comments. We believe the manuscript and package are considerably stronger as a result of taking them into account, and we thank the reviewers for their very helpful and constructive feedback.

\quad

Sincerely,

\quad

Matthieu Bruneaux and Andrés López-Sepulcre

\qquad

\textbf{Note 1:} Line numbers in reviewers' comments refer to the original manuscript submitted to Methods in Ecology and Evolution; in our response, line numbers based on the revised version of the manuscript are shown \hl{highlighted in yellow}.

\quad

\textbf{Note 2:} We are happy to announce that the latest stable version of \texttt{isotracer} (1.0.4) was accepted by the CRAN repository (\url{https://cran.r-project.org/package=isotracer}). We are planning to update the version on CRAN with the development version 1.0.4.9002 if the reviewers approve of the package updates made during the revision process of this manuscript.


%%% ** Comments from the Associate Editor

\section*{From the Associate Editor}

\begin{revquote}
Bruneaux and Lopez-Sepulcre take on an important and complex problem in developing a mathematical framework for the analysis of isotope flow through a network. To a non-specialist, the structure of the paper and associated R package and example descriptions were clear and informative. The expert reviewers raise some issues that need to be addressed, but are in clear agreement about the potential utility of the package, and the way that the package can be applied.
\end{revquote}

We are very glad to hear the good reception of our method and package by both reviewers and by the associate editor. We believe it fills an important application niche, and are committed to make it as useful and accessible as possible. We have put our best efforts to address the issues raised by two reviewers, who have been both constructive and generous with the time invested in examining the package.

%%% ** Comments from Reviewer 1

\clearpage
\section*{From Reviewer 1}

%%% *** Main comments

\begin{revquote}
The paper is clearly written and presents an R package, which can be useful to the ecology modeling community. The main goal of the package is to facilitate the implementation of Bayesian models, wherein the likelihood is based on the solution to a linear ODE. The dynamic of the ODE can be perturbed by various factors, e.g. pulse events, half life decay, etc, which the package handles seamlessly. Use of the package is demonstrated on three illustrative examples. A few points should be clarified so that users can make an informed decision about whether or not to use this tool.

My main concern is the choice of the ODE integrator, namely an Euler method with a fixed step size. The authors are transparent about using this method, rather than Stan's numerical integrators or matrix exponential function; they even recognize some of its limitations, for instance that there is no automatic check for numerical accuracy. This transparency is appreciated. One can add that the Euler method is archaic, fragile, and scarcely used in applied mathematics (there is a reason Stan doesn't implement it), even if it might be appropriate for simple linear ODEs. The ODE defined by Equation~(3) can be solved using matrix exponentials, when y = 0 and more generally when y = constant. When y isn't constant a numerical integrator may be required. Since matrix exponential and stiff and non-stiff numerical integrators are readily available in Stan, a good case should be made for why users of isotracer wouldn't have access to it (or why one of these methods isn't used as a default option).

The argument that the Euler method is "faster" is not very compelling. By that, the authors mean that the execution time is faster. In an MCMC context, we'd at least want to examine the effective sample size per second, rather than just the runtime. But this still wouldn't be good enough if the accuracy of the method isn't first established.  Note that Stan's diagnostic for the inference wouldn't be much help, since your joint distribution is defined using a numerical approximation of the ODE solution, rather than an exact solution. That is you can be doing correct inference on the wrong posterior. With all this ground for skepticism, the paper should clarify why the Euler method is prefered and demonstrate that it provides accurate computation of the joint distribution and its gradient (since HMC is gradient-based).
\end{revquote}

We agree with the reviewer that ensuring the accuracy of any numerical approximations used to calculate likelihood is crucial for sampling of the correct posterior and for valid inferences. We apologize that the previous version of our text might have implied that time efficiency was the primary criteria to choose a method. Historically, at the earlier stages of \texttt{isotracer} development, we implemented matrix exponentials and Stan's numerical ODE solvers but the performance degradations (in terms of runtime) were so severe that running large models was not practically realistic anymore. We thus decided to focus on the forward Euler method, applied with caution. However, the reviewer's comment prompted us to reevaluate the use of matrix exponentials in the latest stable version of our package.

As the reviewer pointed out, the system of ODEs defined by Equation~3 can be solved analytically using matrix exponential if $\mathbf{y}^{(t)} = \mathbf{0}$ or if it is constant. The addition regimes currently implemented in \texttt{isotracer} can all be modelled using step-wise constant $\mathbf{y}^{(t)}$: the timeline of an addition experiment can be separated into segments with constant $\mathbf{y}^{(t)} = \mathbf{0}$ between pulse events. This is also valid for drip regimes, which are currently modelled in \texttt{isotracer} as steady-state compartments to which pulse events are applied to modify the drip value. We thus modified the code of our Stan model to allow the use of matrix exponentials for numerically-accurate solving of the system of ODEs, and we were very happily surprised to see that this change did not result in runtime increases that would make fitting moderately large network models (similar to those used in the Trinidad stream case study) impractical.

Based on this, we modified \texttt{isotracer} so that the core function \verb|run_mcmc()| now uses the matrix exponential method as the default. However, given that runtimes with the matrix exponential solver can still become impractically large for very large models (i.e. large network and/or large number of observation times), we kept our previous constant-step forward Euler scheme as a fallback option if the user explicitly chooses it when running \verb|run_mcmc()|. Having both solver options allows the user to first perform a faster (but less numerically reliable) computation with the Euler method to explore the modelling of large models if they so wish, and to subsequently run them with matrix exponentials for a final run aiming at accurate and reliable results.

We have re-analyzed all our case studies with the now default matrix exponential method, and have updated the main text figures and the tutorial appendix. We updated the text describing the package implementation overview:

\begin{ourquote}
  (Lines \hl{249-271}) The model exposed above is implemented in \verb|isotracer| using a Bayesian approach. The Stan program \citep{carpenter_stan_2017} and its R interface (the \texttt{rstan} package, \citet{rstan_2020}) are used for posterior sampling. Stan uses a Hamiltonian Monte Carlo (HMC) algorithm to sample parameter posteriors with MCMC sampling. The HMC sampling allows for efficient and robust sampling of the posterior, and Stan output provides diagnostics to check that the sampled Markov chains behave appropriately. The Stan model calculates the expected compartment trajectories for a set of parameter values, using matrix exponentials to solve the system of differential equations, and the likelihood is calculated by comparing observations to those expected trajectories. Matrix exponentials can be used to solve the system of differential equations in Equation~3 because all the addition regimes currently implemented by \texttt{isotracer} are equivalent to partitioning the experiment timeline into segments over which $\mathbf{y}^{(t)} = \mathbf{0}$.

Note that, while the matrix exponential approach is numerically accurate and is the default solver used by \texttt{isotracer}, runtime can become prohibitively impractical for large models (i.e. with a large number of compartments and/or a large number of unique observation times). A fallback solver is available to the user: it uses a forward Euler scheme with constant step size, but offers no extensive check of numerical accuracy during the numerical solving. This approach allows for greater speed of model evaluation and is expected to perform robustly when a correct $dt$ time step and reasonable parameter priors are chosen, but numerical accuracy is not guaranteed. As such, it should be used only for prototyping purposes when exploring the modelling of large datasets, and the matrix exponential solver should always be used for final runs.
\end{ourquote}

We also have updated the Concluding Remarks:

\begin{ourquote}
  (Lines \hl{627-635}) From a technical point of view, the default solver for differential equations used by \texttt{isotracer} uses matrix exponentials for numerical accuracy and is reasonably fast (running 2000 MCMC iterations for the Trinidadian stream case study presented here took less than 20 minutes on a 3.6 GHz processor with one core per chain). The runtime with this default solver can however become problematic for larger models, in which case Stan HMC tuning parameters such as \verb|adapt_delta| and \verb|max_treedepth| can be adjusted directly via \verb|run_mcmc()|. An alternative solver using a forward Euler scheme is also offered for exploratory modelling, but without any guarantee of numerical accuracy.
\end{ourquote}

\begin{revquote}
I think it would be helpful to the reader if the authors provided a brief comparison between Stan's and isotracer's interface. What are the limitations of Stan that motivate building isotracer on top of it (e.g. isotracer can read in network, while Stan requires users to write out ODEs, or the matrix A, less coding, etc.)? When might a user decide to switch to Stan (e.g. more flexibility when specifying priors and likelihoods, control over the tuning parameters of HMC, etc.)?
\end{revquote}

We appreciate the reviewer's suggestions and we have now added several remarks about the utility of \texttt{isotracer} relatively to Stan, and when each might be useful. The text in the Concluding Remarks was updated:

\begin{ourquote}
  (Lines \hl{606-626}) The \texttt{isotracer} package implements a Bayesian approach to model tracer addition experiments and to estimate parameter uncertainty rigorously. Its primary aim is to allow researchers whose typical expertise might not cover statistical modelling of systems governed by ordinary differential equations to perform rigorous and reliable statistical analyses, so that the valuable data produced by tracer addition experiments can be fully taken advantage of.
%
The package is built upon the versatile probabilistic programming language Stan, but provides a user-friendly interface to define network structure and experimental design that does not require the user to write any Stan code and makes quick prototyping of models possible.

\texttt{isotracer} allows for modelling of a variety of addition designs (pulse, drip, pulse-chase), can take into account both stable and decaying (e.g. radioactive) tracers, and can be used for model comparison to test hypotheses relating to network structure. The toolkit provided for post-MCMC analyses is rich and can be used to calculate network-wide properties (and their uncertainties) such as flow estimates or compartments steady state sizes when applicable.
%
At the moment, the package does not provide explicit spatial modelling beyond discrete replicate units. However, a continuous handling of the spatial component might be useful to study e.g. stream bed nitrification processes. This type of model would typically involve systems of partial differential equations, which are not implemented for now in the package and for which we recommend the user to use Stan directly, which might require higher expertise in statistical analysis and coding.
\end{ourquote}

We also mention how some of Stan HMC tuning parameters can be modified by \texttt{isotracer} users:

\begin{ourquote}
  (Lines \hl{630-633}) The runtime with this default solver can however become problematic for larger models, in which case Stan HMC tuning parameters such as \verb|adapt_delta| and \verb|max_treedepth| can be adjusted directly via \verb|run_mcmc()|.
\end{ourquote}

%%% *** Minor comments

\subsection*{Minor comments}

\begin{revquote}
Next some minor comments:
- On page 4, justification for the Bayesian framework could be a bit more elaborate. What does it mean to do "statistical rigorous inferences" and why is this specific to a Bayesian framework? Same comment with parameter uncertainty and calculating derived quantities. Yes, I agree the Bayesian framework is extremely useful, but in this section the paper doesn't really explain why. The example do showcase certain strengths of Bayesian inference.
\end{revquote}

We apologize if our text seemed to imply that Bayesian inferences are the only rigorous ones throughout the manuscript. We have now rephrased and clarified, for example, how the ease with which the Bayesian framework deals with marginalization makes it a very useful tool to propagate errors from estimated parameters to derived quantities of interest:

\begin{ourquote}
  (Lines \hl{70-71}) We created the \texttt{isotracer} R package in an effort to develop a statistical framework for such analyses more easily accessible for researchers. [\textellipsis]

  (Lines \hl{81-85}) By using a statistical framework, we can make rigorous inferences and estimate parameter uncertainty. We choose a Bayesian approach to leverage its ability to easily perform marginalization and propagate errors from estimated parameters to derived parameters of interests such as network-wide properties (e.g. total nutrient flow in an ecosystem).
\end{ourquote}

We also rephrased some of the Concluding Remarks:

\begin{ourquote}
  (Lines \hl{673-675}) [\textellipsis] As such, it provides a statistical framework allowing for reliable inferences in comparative or experimental contexts, compared to some previous approaches such as manually solving mass-balance equations iteratively.
\end{ourquote}

\begin{revquote}
- On page 15, the authors propose a default half-Cauchy prior. The reader is encouraged not to rely on this default. I would add that the Cauchy with its heavy tail and infinite variance is prone to frustrate MCMC algorithms and is generally not recommended -- not even to produce a weakly informative prior. What's more, the scale and earlier discussion in the paper suggest that the authors recommend informative priors.
\end{revquote}

We appreciate the reviewer's concerns, and this point is also related to some comments from the second reviewer about the trust that package users might put in default priors. A good choice of priors is very important for reliable Bayesian inference, and providing default priors for user-friendliness might actually be detrimental to the final user if they do not approach them critically.

To address this concern, we made several important changes: (i) we decided to altogether remove default priors, so that users always have to explicitly choose and set their own priors for their model; (ii) we have expanded the range of available priors in \texttt{isotracer}, now adding exponential and gamma distributions; (iii) we updated the tutorial appendix and the package documentation by not using the half-Cauchy prior in the examples anymore but instead relying mostly on normal priors, and also by providing some justification for our prior choice in the documentation that can guide the user to make their own choices.

We have updated the main text in several places as a results:

\begin{ourquote}
  (``Model definition - A minimal model'', lines \hl{317-323}) Importantly, while many packages implementing Bayesian methods provide default priors, \texttt{isotracer} does not: the user has to explicitly set priors for all parameters. This is a conscious design choice made to encourage users to make reasoned, explicit choices about their model priors, rather than trusting some default priors that might be woefully inappropriate for the system being modelled. The tutorials presented in appendix and the package documentation provide some guidance for choosing slightly informative priors.
\end{ourquote}

\begin{ourquote}
  (``Model definition - Statistical model properties'', lines \hl{359-362})
  \begin{itemize}
  \item \verb|set_prior()| to set prior distributions for individual model parameters. Implemented priors are Uniform, Normal, half-Cauchy, Exponential, Gamma, and scaled Beta distribution. All priors are truncated to 0. Additionally, a ``constant'' prior is available to fix the value of some parameters during MCMC.
  \end{itemize}
\end{ourquote}

\begin{ourquote}
  (Table note [d] in Table 1) Note that \texttt{isotracer} does not provide any default prior: the user must explicitly set priors for all model parameters. Tutorials in the appendix and package documentation provide guidance for reasonable prior choices.
\end{ourquote}

%%% ** Comments from Reviewer 2 (Prof. Bob Hall)

\section*{From Reviewer 2 (Prof. Bob Hall)}

%%% *** Overview

\subsection*{Overview}

\begin{revquote}
This manuscript describes a R package for tracing flows of isotope (or any other tracer) through a network. I have been waiting 27 years for this paper and the software, we have used all sort of ways of modeling isotope flows including Excel, ouch. This paper would have changed my life as a grad student in 1995, and now may change my life as a someone who loves these sorts of tracer experiments. My review will comprise a discussion of the paper and a quick data faking excercise of a toy food web. As one who knows a lot about tracer studies, especially stream tracer additions, a medium amount about modeling and Bayesian confrontation using Stan, and 15 years of self-taught bad R skills, I feel that I represent at least the median user of this program. And in fact, Isotracer motivates me to return to using to isotope studies. Thus my ability to use (or not) this software ought to be similar to others. I signed my name, so if the authors want to contact me with any questions, they are welcome to do so.

\vspace{\baselineskip}
\textbf{Things I liked}
\begin{enumerate}
\item Use of Stan. I only use this platform for complicated multilevel modeling projects.

\item Data simulation. All models should go through a fake data test before seeing any real data, and the authors explicitly built this capability into isotracer. This point cannot be made strongly enough to future users. Fake the data, run the model, then apply to real data.

\item Documentation, I spent more time on reading the excellent online documentation than the paper. It is very useful step by step approach. That said, I had a lot of hiccups in programming, mostly because I am not very good at it.

\item That the software is general enough to be used beyond the stream tracer experiments that both the authors and I do. The examples they showed clearly made that point.
\end{enumerate}
\end{revquote}

Thank you for the very positive feedback. We are delighted to read that a renowned expert in tracer additions finds this tool so valuable.

\begin{revquote}
\textbf{Big question}

How does Isotracer handle space? Yes it can do several transects and partially pool these, but in streams we have variation in space (which is to say, short term time) that enables estimating rates. Examples:

\begin{enumerate}
\item filter feeding caddisflies near a drip site will have less 15N than those downstream because most fo the food for these close caddisflies comes from the unlabelled reach. I supppose one accounts for this by the biomass time the fraction labeled in seston? Or can one incorporate a model of seston turnover?

\item The downstream decline of NH4 gives the uptake rate (in 1/distance, converted using velocity to 1/time) of NH4. Can one use this uptake rate in the isotracer model? Maybe enforce a strong prior with this measured downstream rate?

\item Other useful models measure say conversion of NO 3 to N 2 gas (Mullholland et al 2004 L\&O). In this case we use the downstream decline in NO 3 and the downstream production of N 2 for the rate. In another case is mineralization of DON from the stream bottom, post tracer (Johnson et al. 2013, L\&O), where we model DON release from a measured pattern of downstream 15 N on the bottom of the stream.
\end{enumerate}

I realize what am asking for here is a partial differential equation model which is a completly different beast than the ODE solver in Isotracer. No way am I asking for that, but rather consideration of these approaches and what one would do to model them using Isotracer, even if the answer is "don't use Isotracer, you are on your own".
\end{revquote}

For now, \texttt{isotracer} does not handle continuous spatial information at all, and only supports some discrete spatial designs by using separate transects as replicates in the network model. To answer more specifically to the examples above:
\begin{enumerate}
  
\item Seston carried by stream flow in this example is indeed difficult to model with \texttt{isotracer}, because its input are not local to the transects being modelled. Once approach could be to treat seston as a source compartment itself, for which enrichment and biomass trajectories within a transect are not modelled but are taken as a given from observations. This would be similar to the approach we used in \citet{lopez-sepulcre_15N_2020} for NO$_3^-$, where the drip regime consisted in adding $^{15}$N-NH$_4^+$ but where we modelled both NH$_4^+$ and NO$_3^-$ as steady-state source compartments instead of explicitly modelling nitrification. This was a reasonable approximation, as the observed NO$_3^-$ profiles were responding very quickly to the NH$_4^+$ drip regime and could be themselves modelled by step functions. In the case of seston, however, a smoother approximation would probably be needed rather than a step function with only two states (drip on/drip off).

\item In our understanding, the uptake rate of NH$_4^+$ measured from the downstream decline of NH$_4^+$ in the water would represent the sum of ammonium uptake by all compartments using it, i.e. all primary consumers (including possibly some stream bed compartments). If only one such primary consumer compartment exists, then a tight prior might be used to enforce this measured downstream rate as an uptake rate from dissolved NH$_4^+$ into the unique primary consumer. However, if more than one primary consumer exists, then a more sophisticated modelling approach would be required to impose a constraint on the sum of the uptake rates by all those primary consumers, so that this sum is equal to the measured downstream rate for NH$_4^+$. This should be possible with Stan by using a unit simplex for those uptake rates, but is not implemented in \texttt{isotracer}.

\item In models where downstream decline of NO$_3^-$ and downstream production of N$_2$ are measured, or where downstream pattern of $^{15}$N on the bottom of stream is measured, we understand that the spatial coordinates (i.e. the distances from the drip source to the sampling locations) must be explicitly included in the model through partial differential equations to handle this data. This is not currently implemented in \texttt{isotracer}.
\end{enumerate}

We completely agree with Prof. Bob Hall on the importance of spatial structure for this type of ecosystem studies. In fact, the explicit incorporation of continuous spatial information is our next priority for a following version of the \texttt{isotracer} package, and this type of feedback from the research community is a great input to help us prioritize which features implementation should be worked on. For the moment, as the reviewer suggested, we simply added some text to inform users that may require such explicit treatment of space that they would have to use Stan directly:

\begin{ourquote}
  (``Concluding remarks'', lines \hl{xxx-xxx}) At the moment, the package does not provide explicit spatial modelling beyond discrete replicate units. However, a continuous handling of the spatial component might be useful to study e.g. stream bed nitrification processes. This type of model would typically involve systems of partial differential equations, which are not implemented for now in the package and for which we recommend the user to use Stan directly, which might require higher expertise in statistical analysis and coding.
\end{ourquote}

%%% *** Specific comments

\subsection*{Specific comments}

\begin{revquote}
Fig 1 caption. The authors call mass/time a rate. This is not wrong, but it may be clearer to call a rate any 1/time unit and call mass/time a flux, so $y_1$ is a flux, $\lambda_1$ is a rate.
\end{revquote}

We agree with this and we have updated the caption for Figure~1:

\begin{ourquote}
  (Caption for Figure~1) Example of a basic network with three compartments. The $x_i$ values in each compartment are quantities of matter. The values along arrows are fluxes (quantity per time unit). $y_1$ is a flux (quantity per time unit); $\upsilon_{i,j}$ and $\lambda_i$ are rate coefficients of first-order kinetics (per time unit), which we simply call ``rates'' in this study for convenience. $x_i$ and $y_1$ are functions of time $t$ while $\upsilon_{i,j}$ and $\lambda_i$ are constant rates.
\end{ourquote}

We also have changed this throughout the manuscript (i.e., using ``flux'' when writing about a mass/time unit and ``rate'' when writing about a 1/time unit).

\hl{MB: Change it throughout the manuscript.}

\begin{revquote}
165 \verb|``| for left quotes.
\end{revquote}

Thank you for noticing, this is now corrected.

\begin{revquote}
258. OK I see the model is discretized here. No problem, just observing.
\end{revquote}

This was required for the forward Euler solver, which is still available as an option but is not the default anymore. Under advice from Reviewer 1, our default method to solve the ODE system now uses matrix exponentials, which does not require discretization, and provides numerically accurate results to ensure that the correct posterior is sampled by Stan. (See also our response to Reviewer 1 above.)

\begin{revquote}
259. This point cannot be made strongly enough. I am coauthor on a R package for estimating stream metabolism by Bayesian methods, and more often that no users accept the default priors, mostly because they are unsure of that they need to specify a prior and then how to do it. A first semester course in applied Bayesian stats (or a close read of Hobbs and Hooten) will greatly help users of this program.
\end{revquote}

We agree. In fact the biggest changes to the \texttt{isotracer} package made during these revisions have been (i) to make the solver more numerically robust to parameter priors by using Stan's implementation of matrix exponentials to solve the system of ODEs governing a network model, and (ii) to no longer have default priors set by \verb|new_networkModel()|.

As explained in our response to Reviewer 1's comment about half-Cauchy priors above, we think that removing default priors will ultimately be beneficial for the package users as it will force them to explicitly reflect and choose priors that are appropriate for their model. We tried to provide guidance for reasonable prior choices in the package documentation and in the Tutorials appendix of this manuscript, and we also added two new priors to the package for more versatility (exponential and gamma distributions). We hope that by doing so, we can promote a better awareness of the importance of prior choice among the end users, and discourage the blind use of default priors.

\begin{revquote}
370, 375. Same advice as 314, checking theses models from computational problems to PP checks is essential and not often well understood by users My first run tanked (i.e. zero chain mixing), I think because my biomass `data' had no error.
\end{revquote}

We agree that posterior predictive checks are crucial to validate a Bayesian model. We hope that providing an easy-to-use toolkit for PP checks as part of \texttt{isotracer}  and emphasizing its use in the package documentation will encourage users to systematically perform those checks for their models.

\begin{revquote}
Fig. 6 Lovely.
\end{revquote}

Thank you!

\begin{revquote}
References: Here are some papers the authors may wish to consider for historical connection and a new application: Crossley 1967. Earliest paper that I know of that examines first order food web tracer studies and their time course. https://doi.org/10.2307/1294518

Wollheim et al 1999. A non-statistical version of the modeling approach in this paper. https://doi.org/10.2307/1468461

Barneche et al 2021. Cool new paper whose data could have been (more) easily modeled with isotracer. They too had an explicitly multilevel modeling approach https://doi.org/10.1038/s41586-021-03352-2
\end{revquote}

We are grateful for the useful references. We now justly cite \citet{crossley_analysis_1969} and \citet{wollheim_coupled_1999} in the introduction (lines \hl{xxx} and \hl{xxx}), and we have added \citet{barneche_warming_2021} as an example of experimental use of tracer studies (line \hl{xxx}).

\begin{revquote}
\textbf{Comment for vignette}

add code: options(mc.cores = parallel::detectCores()) to the vignette.
\end{revquote}

Thank you for help to improve the documentation. We now have included a start-up message in \texttt{isotracer} (similar to what \texttt{rstan} does):

\begin{ourquote}
\begin{verbatim}
> library(isotacer)

To automatically run isotracer in parallel on a multicore
CPU, you can call:
  options(mc.cores = parallel::detectCores())
\end{verbatim}
  
\end{ourquote}

%%% *** Fake data exercise

\subsection*{Fake data exercise}

We are very grateful for the Reviewer's efforts in testing the package with fake data and for his positive comments on the use of the package.

At the end of this fake data exercise, the Reviewer noted that the credible intervals he obtained for the uptake rates of his model were quite large, and that only two out of three credible intervals did contain the true parameter value. We have reproduced the Reviewer's analysis, and it seems that this discrepancy is due to a slight mistake in setting the initial condition of the network model.

We provide below (after the Bibliography section) an Rmarkdown document reproducing Reviewer 2's fake data exercise and in which we rerun the model after adjusting the initial conditions of the model. We were happy to see that the credible intervals became much narrower and that they contained the true parameter value for all three uptake rates (\verb|upsilon_*| parameters).

%%% * References

\bibliographystyle{../../current-manuscript/my-plainnat}
\bibliography{../../current-manuscript/biblio-curated}

\end{document}
