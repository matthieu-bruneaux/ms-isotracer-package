### * Setup

TOP_DIR = $(shell git rev-parse --show-toplevel)
TARGET = "response-letter-draft"

### ** Colors

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://siongui.github.io/2016/02/18/makefile-echo-color-output/#id3
RED = "\\033[31m"
GREEN = "\\033[92m"
NC = "\\033[0m"

### * Rules

### ** help

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@printf "\n"
	@printf "Please use 'make <target>' where <target> is one of\n"
	@printf "\n"
	@sed -n 's/^## /    /p' $< | column -t -s ":"

### ** pdf

## pdf : build the pdf
.PHONY: pdf
pdf:
	@printf "\n"
	@printf "$(GREEN)*** Building the pdf ***$(NC)\n"
	@printf "\n"
	pdflatex $(TARGET).tex
	bibtex $(TARGET)
	pdflatex $(TARGET).tex
	pdflatex $(TARGET).tex
	pdflatex $(TARGET).tex
	pdfunite $(TARGET).pdf response-to-reviewer-2-exercise.pdf $(TARGET)-with-Rmd.pdf
	mv $(TARGET)-with-Rmd.pdf $(TARGET).pdf

### ** clean

## clean : remove all automatically generated files
.PHONY: clean
clean: clean-tmp clean-pdf
	@printf "\n"
	@printf "$(GREEN)*** Complete cleaning ***$(NC)\n"
	@printf "\n"

# clean-tmp : remove the helper files automatically generated
.PHONY: clean-tmp
clean-tmp:
	@rm -f *.aux *.log *.out *.bbl *.blg *.dvi *.toc *.lof *.lot

# clean-pdf : remove the target pdf
.PHONY: clean-pdf
clean-pdf:
	@rm -f $(TARGET).pdf

### ** all

## all : clean and build pdf (and remove other output files)
.PHONY: all
all: clean pdf
	@make clean-tmp
