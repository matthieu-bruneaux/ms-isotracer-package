* General information

- MEE response received on 2021-08-10.
- E-mail subject: "Methods in Ecology and Evolution - Decision on Manuscript ID MEE-21-08-630"
- "Please also ensure that your altered manuscript still conforms to our word limit of 6000-7000 for Research Articles, or 3000 for Applications or Practical Tools articles."
- "Once you have made the suggested changes, go to https://mc.manuscriptcentral.com/mee-besjournals and login to your Author Centre. Click on "Manuscripts with Decisions," and then click on "Create a Resubmission" located next to the manuscript number. Then, follow the steps for resubmitting your manuscript."

* Comments from the Associate Editor

Bruneaux and Lopez-Sepulchre take on an important and complex problem in developing a mathematical framework for the analysis of isotope flow through a network. To a non-specialist, the structure of the paper and associated R package and example descriptions were clear and informative. The expert reviewers raise some issues that need to be addressed, but are in clear agreement about the potential utility of the package, and the way that the package can be applied.

* Comments from Reviewer 1

The paper is clearly written and presents an R package, which can be useful to the ecology modeling community. The main goal of the package is to facilitate the implementation of Bayesian models, wherein the likelihood is based on the solution to a linear ODE. The dynamic of the ODE can be perturbed by various factors, e.g. pulse events, half life decay, etc, which the package handles seamlessly. Use of the package is demonstrated on three illustrative examples. A few points should be clarified so that users can make an informed decision about whether or not to use this tool.

My main concern is the choice of the ODE integrator, namely an Euler method with a fixed step size. The authors are transparent about using this method, rather than Stan's numerical integrators or matrix exponential function; they even recognize some of its limitations, for instance that there is no automatic check for numerical accuracy. This transparency is appreciated. One can add that the Euler method is archaic, fragile, and scarcely used in applied mathematics (there is a reason Stan doesn't implement it), even if it might be appropriate for simple linear ODEs. The ODE defined by Equation (3) can be solved using matrix exponentials, when y = 0 and more generally when y = constant. When y isn't constant a numerical integrator may be required. Since matrix exponential and stiff and non-stiff numerical integrators are readily available in Stan, a good case should be made for why users of isotracer wouldn't have access to it (or why one of these methods isn't used as a default option).

The argument that the Euler method is "faster" is not very compelling. By that, the authors mean that the execution time is faster. In an MCMC context, we'd at least want to examine the effective sample size per second, rather than just the runtime. But this still wouldn't be good enough if the accuracy of the method isn't first established.  Note that Stan's diagnostic for the inference wouldn't be much help, since your joint distribution is defined using a numerical approximation of the ODE solution, rather than an exact solution. That is you can be doing correct inference on the wrong posterior. With all this ground for skepticism, the paper should clarify why the Euler method is prefered and demonstrate that it provides accurate computation of the joint distribution and its gradient (since HMC is gradient-based). 

I think it would be helpful to the reader if the authors provided a brief comparison between Stan's and isotracer's interface. What are the limitations of Stan that motivate building isotracer on top of it (e.g. isotracer can read in network, while Stan requires users to write out ODEs, or the matrix A, less coding, etc.)? When might a user decide to switch to Stan (e.g. more flexibility when specifying priors and likelihoods, control over the tuning parameters of HMC, etc.)?

Next some minor comments:
- On page 4, justification for the Bayesian framework could be a bit more elaborate. What does it mean to do "statistical rigorous inferences" and why is this specific to a Bayesian framework? Same comment with parameter uncertainty and calculating derived quantities. Yes, I agree the Bayesian framework is extremely useful, but in this section the paper doesn't really explain why. The example do showcase certain strengths of Bayesian inference.
- On page 15, the authors propose a default half-Cauchy prior. The reader is encouraged not to rely on this default. I would add that the Cauchy with its heavy tail and infinite variance is prone to frustrate MCMC algorithms and is generally not recommended -- not even to produce a weakly informative prior. What's more, the scale and earlier discussion in the paper suggest that the authors recommend informative priors.

* Comments from Reviewer 2 (Bob Hall, from attached rmarkdown file)

** Overview

This manuscript describes a R package for tracing flows of isotope (or any other tracer) through a network. I have been waiting 27 years for this paper and the software, we have used all sort of ways of modeling isotope flows including Excel, ouch. This paper would have changed my life as a grad student in 1995, and now may change my life as a someone who loves these sorts of tracer experiments. My review will comprise a discussion of the paper and a quick data faking excercise of a toy food web. As one who knows a lot about tracer studies, especially stream tracer additions, a medium amount about modeling and Bayesian confrontation using Stan, and 15 years of self-taught bad R skills, I feel that I represent at least the median user of this program. And in fact, Isotracer motivates me to return to using to isotope studies. Thus my ability to use (or not) this software ought to be similar to others. I signed my name, so if the authors want to contact me with any questions, they are welcome to do so.

*** Things I liked

1. Use of Stan. I only use this platform for complicated multilevel modeling projects.

2. Data simulation. All models should go through a fake data test before seeing any real data, and the authors explicitly built this capability into isotracer. This point cannot be made strongly enough to future users. Fake the data, run the model, then apply to real data.

3. Documentation, I spent more time on reading the excellent online documentation than the paper. It is very useful step by step approach. That said, I had a lot of hiccups in programming, mostly because I am not very good at it.

4. That the software is general enough to be used beyond the stream tracer experiments that both the authors and I do. The examples they showed clearly made that point.

*** Big question

How does Isotracer handle space? Yes it can do several transects and partially pool these, but in streams we have variation in space (which is to say, short term time) that enables estimating rates. Examples:

1. filter feeding caddisflies near a drip site will have less 15N than those downstream because most fo the food for these close caddisflies comes from the unlabelled reach. I supppose one accounts for this by the biomass time the fraction labeled in seston? Or can one incorporate a model of seston turnover?

2. The downstream decline of NH4 gives the uptake rate (in 1/distance, converted using velocity to 1/time) of NH4. Can one use this uptake rate in the isotracer model? Maybe enforce a strong prior with this measured downstream rate?

3. Other useful models measure say conversion of NO 3 to N 2 gas (Mullholland et al 2004 L&O). In this case we use the downstream decline in NO 3 and the downstream production of N 2 for the rate. In another case is mineralization of DON from the stream bottom, post tracer (Johnson et al. 2013, L&O), where we model DON release from a measured pattern of downstream 15 N on the bottom of the stream.

I realize what am asking for here is a partial differential equation model which is a completly different beast than the ODE solver in Isotracer. No way am I asking for that, but rather consideration of these approaches and what one would do to model them using Isotracer, even if the answer is "don't use Isotracer, you are on your own".

** Specific comments

Fig 1 caption. The authors call mass/time a rate. This is not wrong, but it may be clearer to call a rate any 1/time unit and call mass/time a flux, so
$y_1$ is a flux, $\lambda_1$ is a rate.

165 `` for left quotes.

258. OK I see the model is discretized here. No problem, just observing.

259. This point cannot be made strongly enough. I am coauthor on a R package for estimating stream metabolism by Bayesian methods, and more often that no users accept the default priors, mostly because they are unsure of that they need to specify a prior and then how to do it. A first semester course in applied Bayesian stats (or a close read of Hobbs and Hooten) will greatly help users of this program.

370, 375. Same advice as 314, checking theses models from computational problems to PP checks is essential and not often well understood by users My first run tanked (i.e. zero chain mixing), I think because my biomass `data' had no error.

Fig. 6 Lovely.

References: Here are some papers the authors may wish to consider for historical connection and a new application: Crossley 1967. Earliest paper that I know of that examines first order food web tracer studies and their time course. https://doi.org/10.2307/1294518

Wollheim et al 1999. A non-statistical version of the modeling approach in this paper. https://doi.org/10.2307/1468461

Barneche et al 2021. Cool new paper whose data could have been (more) easily modeled with isotracer. They too had an explicitly multilevel modeling approach https://doi.org/10.1038/s41586-021-03352-2

*** Comment for vignette

add code: options(mc.cores = parallel::detectCores()) to the vignette.

** Fake data exercise

(See the original reviewer's pdf generated from rmarkdown.)

* Matthieu's notes

About half-Cauchy prior: see https://www.rdocumentation.org/packages/extraDistr/versions/1.9.1/topics/HalfCauchy
